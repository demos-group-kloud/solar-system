kubectl -n ${NAMESPACE} create secret generic mongo-db-creds --from-literal=MONGO_URI=$MONGO_URI  \
  --from-literal=MONGO_USERNAME=$MONGO_USERNAME --from-literal=MONGO_PASSWORD=$MONGO_PASSWORD \
  --save-config --dry-run=client -o yaml | kubectl apply -f -

